import QtQuick 2.2
import QtQuick.Controls 1.1
import QtQuick.Window 2.2
import Hack 1.0

ApplicationWindow {
    title: "HACK"
    visible: true
    width: Screen.width
    height: Screen.height

    QHackModel {
	id: hackmodel
    }
    
    TableView {
	anchors.fill: parent
	TableViewColumn {
            role: "name"
            title: "Name"
        }
	TableViewColumn {
            role: "frameNumber"
            title: "Frame Number"
        }

	model: hackmodel.hacks
	// model: ListModel {
	//     ListElement {
	// 	name: "Bill Smith"
	// 	frameNumber: "555 3264"
	//     }
	//     ListElement {
	// 	name: "John Brown"
	// 	frameNumber: "555 8426"
	//     }
	//     ListElement {
	// 	name: "Sam Wise"
	// 	frameNumber: "555 0473"
	//     }
	// }
    }
}
