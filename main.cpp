#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QList>

#include "hack_model.hpp"

// Q_DECLARE_METATYPE(QHack)
// Q_DECLARE_METATYPE(QQmlListProperty<QHack>)

int main(int argc, char *argv[]) {
  QGuiApplication app(argc, argv);
  qmlRegisterType<QHackModel>("Hack", 1, 0, "QHackModel");
  qmlRegisterType<QHack>("Hack", 1, 0, "QHack");
  QQmlApplicationEngine engine(QUrl("qrc:///hack.qml"));
  return app.exec();
}
