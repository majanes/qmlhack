#ifndef _QHACKMODEL_HPP_
#define _QHACKMODEL_HPP_

#include <QObject>
#include <QString>
#include <QQmlListProperty>

class QHack : public QObject {
  Q_OBJECT
  Q_PROPERTY(QString name READ name CONSTANT);
  Q_PROPERTY(QString frameNumber READ frameNumber CONSTANT);
 public:
  QHack() {}
  QHack(const QHack&) {}
  QString name() { return "foo";}
  QString frameNumber() { return "123";}
};

class QHackModel : public QObject {
  Q_OBJECT
  Q_PROPERTY(QQmlListProperty<QHack> hacks READ hacks NOTIFY onHacks);
 public:
  QHackModel();
  ~QHackModel();

  QQmlListProperty<QHack> hacks();
 signals:
  void onHacks();
 private:
  static int hack_count(QQmlListProperty<QHack> *property);
  static QHack *hack_at(QQmlListProperty<QHack> *property, int i);


  std::vector<QHack *> m_hacks;
};

#endif
