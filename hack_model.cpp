#include "hack_model.hpp"

QHackModel::QHackModel() {
  for (int i = 0; i < 10; ++i)
    m_hacks.push_back(new QHack());
  // emit onHacks();
}
QHackModel::~QHackModel() {}

QQmlListProperty<QHack>
QHackModel::hacks() {
  // also fails the same way if you use the quick/dirty constructor:
  // QQmlListProperty<QHack>(QList<QHack*>)
  return QQmlListProperty<QHack>(this, this,
                                 &QHackModel::hack_count,
                                 &QHackModel::hack_at);
}

int
QHackModel::hack_count(QQmlListProperty<QHack> *property) {
  QHackModel *m = reinterpret_cast<QHackModel *>(property->data);
  return m->m_hacks.size();
}

QHack *
QHackModel::hack_at(QQmlListProperty<QHack> *property, int i) {
  QHackModel *m = reinterpret_cast<QHackModel *>(property->data);
  return m->m_hacks[i];
}
